﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class AssignProject : Form
    {
        public AssignProject()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" SELECT ProjectId, GroupId, AssignmentDate FROM GroupProject WHERE GroupId = GroupId AND AssignmentDate = ( SELECT MAX(AssignmentDate) FROM GroupProject WHERE GroupId = GroupId)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    
        private void Back_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Close();
        }
        private void Addproject_Click(object sender, EventArgs e)
        {
            string mode = "AddAssignProject";
            double fl = 0.2;
            Form n = new AssignedIDForm(mode, fl);
            n.Show();
            this.Close();

        }

        private void Update_Click(object sender, EventArgs e)
        {
            string mode = "updateAssignProject";
            Char fl = 'A';
            Form n = new AssignedIDForm(mode, fl);
            n.Show();
            this.Close();
        }
    }
}
