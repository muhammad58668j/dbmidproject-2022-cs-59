﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class AddGroup : Form
    {
        public AddGroup()
        {
            InitializeComponent();
        }

      
        private void creatGroup(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkGroupCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM [Group] WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkGroupCmd.Parameters.AddWithValue("@Id", groupid.Text);
            int GroupExists = (int)checkGroupCmd.ExecuteScalar();

            SqlCommand checkStudentCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Student WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkStudentCmd.Parameters.AddWithValue("@Id", studentid.Text);
            int StudentExists = (int)checkStudentCmd.ExecuteScalar();

            if (GroupExists == 1 && StudentExists == 1)
            {
                SqlCommand Cmd = new SqlCommand("INSERT INTO GroupStudent (GroupId, StudentId, Status, AssignmentDate) VALUES (@GroupId, @StudentId, @Status, @AssignmentDate)", con);
                Cmd.Parameters.AddWithValue("@GroupId", groupid.Text);
                Cmd.Parameters.AddWithValue("@StudentId", studentid.Text);
                Cmd.Parameters.AddWithValue("@Status", int.Parse(status.Text));
                Cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);

                Cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully inserted into GroupStudent.");
            }
            else
            {
                MessageBox.Show("Please Enter the Valid ID");
            }

        }

        private void submit_Click(object sender, EventArgs e)
        {
            creatGroup(sender, e);
            Form f = new GroupStudent();
            this.Close();
        }

    
    }

}
