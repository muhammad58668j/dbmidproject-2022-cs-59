﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class UpdateGroupEvaluation : Form
    {
        private int advisorid;
        private int projectid;
        public UpdateGroupEvaluation(int advisorid, int projectid)
        {
            InitializeComponent();
            this.advisorid = advisorid;
            this.projectid = projectid;
            
            IDLabel.Text = $"AdvisorId:{advisorid}+ And PId:{projectid} ";
            IDLabel.ForeColor = System.Drawing.Color.Red;
        }

        private void submit_Click(object sender, EventArgs e)
        {

            updategroupevaluation(sender, e);
        }
        private void updategroupevaluation(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkGroupCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM GroupEvaluation WHERE GroupId = @GroupId AND EvaluationId=EvaluationId) SELECT 1 ELSE SELECT 0", con);
            checkGroupCmd.Parameters.AddWithValue("@GroupId", advisorid);
            checkGroupCmd.Parameters.AddWithValue("@EvaluationId", projectid);
            int GroupExists = (int)checkGroupCmd.ExecuteScalar();


            if (GroupExists == 1 )
            {
                SqlCommand cmd = new SqlCommand("UPDATE GroupEvaluation SET ObtainedMarks=ObtainedMarks, EvaluationDate=@EvaluationDate WHERE GroupId = @GroupId AND EvaluationId=EvaluationId", con);
                cmd.Parameters.AddWithValue("@GroupId", advisorid);
                cmd.Parameters.AddWithValue("@EvaluationId", projectid);
                cmd.Parameters.AddWithValue("@ObtainedMarks",obtainedmarks.Text);
                cmd.Parameters.AddWithValue("@EvaluationDate", DateTime.Now);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated into  Group Evaluation");
            }
            else
            {
                MessageBox.Show("Please Enter the Valid ID");
            }
        }

        private void UpdateGroupEvaluation_Load(object sender, EventArgs e)
        {

        }
    }
}
