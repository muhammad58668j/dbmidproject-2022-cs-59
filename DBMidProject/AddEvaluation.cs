﻿using Guna.UI2.WinForms.Suite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class AddEvaluation : Form
    {
        private OperationMode Mode;
        private int Id;
        private string type;

        public AddEvaluation(int id, OperationMode mode)
        {
            InitializeComponent();
            Id = id;
            Mode = mode;
            IDLabel.Text = $"Evaluation ID: {Id}";
            IDLabel.ForeColor = System.Drawing.Color.Red;

        }


            public AddEvaluation(OperationMode mode)
        {
            InitializeComponent();

            Mode = mode;


        }
        private void submit_Click(object sender, EventArgs e)
        {
            if (Mode == OperationMode.Update)
            {
                updateEvaluation(sender, e);
            }
            else if (Mode == OperationMode.Add)
            {
                addEvaluation(sender, e);
            }
        }
        void addEvaluation(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Evaluation VALUES (@Name,@TotalMarks,@TotalWeightage); SELECT SCOPE_IDENTITY();", con);
            cmd.Parameters.AddWithValue("@Name", Name.Text);
            cmd.Parameters.AddWithValue("@TotalMarks", totalmarks.Text);
            cmd.Parameters.AddWithValue("@TotalWeightage", waitages.Text);
            int id = Convert.ToInt32(cmd.ExecuteScalar());
            MessageBox.Show("Successfully saved into Evaluation");
            IDLabel.Text = $"Evaluation ID: {id}";
            IDLabel.ForeColor = System.Drawing.Color.Red;
        }
        

        private void updateEvaluation(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand checkEvaluationCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Evaluation WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkEvaluationCmd.Parameters.AddWithValue("@Id", Id);

            int EvaluationExists = (int)checkEvaluationCmd.ExecuteScalar();

            if (EvaluationExists == 1)
            {


                SqlCommand cmd = new SqlCommand("UPDATE Evaluation SET Name = @Name ,TotalMarks=@TotalMarks,TotalWeightage=@TotalWeightage WHERE Id = @Id", con);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@Name", Name.Text);
                cmd.Parameters.AddWithValue("@TotalMarks", totalmarks.Text);
                cmd.Parameters.AddWithValue("@TotalWeightage", waitages.Text);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully updated Evaluation data");
            }
            else
            {
                MessageBox.Show("Evaluation with the provided Id does not exist");
            }

        }
    }
}
