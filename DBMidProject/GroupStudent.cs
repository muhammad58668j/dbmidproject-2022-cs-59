﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace mid_project
{
    public partial class GroupStudent : Form
    {
        public GroupStudent()
     
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT S.GroupId, S.StudentId,p.ProjectId, l.Value, S.AssignmentDate FROM GroupStudent S JOIN Lookup l ON S.Status = l.Id" +
                " JOIN GroupProject p ON p.GroupId=S.GroupId WHERE l.Value = 'Active'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }
        public static DataTable GetStuFromGid(int Gid)
        {
            var con = Configuration.getInstance().getConnection();
            
            using (DataTable dt = new DataTable("Student"))
            {
                using (SqlCommand cmd = new SqlCommand("Select p.id,p.FirstName,p.LastName,s.RegistrationNo,p.Contact,p.Email " +
                    "from student as s " +
                    "join person as p on s.id = p.id " +
                    "join lookup as l on p.gender = l.id " +
                    "join GroupStudent as GS on GS.StudentId = P.Id " +
                    "where GS.GroupId = @id ", con))
                {
                    cmd.Parameters.AddWithValue("id", Gid);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
                return dt;
            }
        }
        private void AddGroup_Click(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;
            var con = Configuration.getInstance().getConnection();
            SqlCommand Cmd = new SqlCommand("INSERT INTO [Group] (Created_On) VALUES (@Created_On)", con);
            Cmd.Parameters.AddWithValue("@Created_On", time); 
            Cmd.ExecuteNonQuery();

            MessageBox.Show("Successfully inserted into Group");


        }
        private void Back_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Close();
        }
        private void Add_Click(object sender, EventArgs e)
        {

            Form n = new AddGroup();
            n.Show();
            this.Close();


        }


   

        private void Delete_Click(object sender, EventArgs e)
        {
            int id=3;

            string mod = "deleteGroup";
            Form Idform = new AssignedIDForm(mod,id);
            Idform.Show();
            this.Close();

        }

        private void GroupStudent_Load(object sender, EventArgs e)
        {

        }
    }
}
