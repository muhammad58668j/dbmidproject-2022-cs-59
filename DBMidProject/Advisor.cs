﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace mid_project
{
    public partial class Advisor : Form
    {
        public Advisor()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * FROM Advisor WHERE Salary>0", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        public static DataTable GetAdvisorTableDetails()
        {
           var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.id,p.FirstName,p.LastName,lo.Value AS Designation,s.Salary,p.Contact,p.Email,p.DateOfBirth,l.Value as Gender " +
                "from Advisor as s " +
                "join person as p on s.id = p.id " +
                "join lookup as l on p.gender = l.id " +
                "join lookup as lo on s.Designation=lo.Id ", con);
            DataTable dt = new DataTable();
            
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            
            return dt;
        }
        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Back_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Close();
        }
        private void Add_Click(object sender, EventArgs e)
        {
           
            Form n = new AddAdvisor(OperationMode.Add);
            n.Show();
            this.Close();


        }


        private void Update_Click(object sender, EventArgs e)
        {
            string mode = "updateadvisor";
            Form Idform = new IDFormcs(mode);
            Idform.Show();
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {

            string mod = "deleteadvisor";
            Form Idform = new IDFormcs(mod);
            Idform.Show();
            this.Close();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    
    }
}
