﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public enum OperationMode
    {
        Add,
        Update
    }

    public partial class AddForm : Form
    {
        private OperationMode Mode;
        private int Id;
        public AddForm(int id,OperationMode mode)
        {
            InitializeComponent();
            Id = id;
            Mode = mode;
            IDLabel.Text = $"Your ID: {Id}";
            IDLabel.ForeColor = System.Drawing.Color.Red;

        }
    
        public AddForm(OperationMode mode)
        {
            InitializeComponent();
            Mode = mode;
        }

        private void submit_Click(object sender, EventArgs e)
        {
            
                if (Mode == OperationMode.Add)
                {
                    AddStudent(sender, e);
                }
                else if (Mode == OperationMode.Update)
                {
                    update(sender, e);
                }
           
        }


        private void update(object sender,EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand checkStudentCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Student WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkStudentCmd.Parameters.AddWithValue("@Id", Id);

            int studentExists = (int)checkStudentCmd.ExecuteScalar();

            if (studentExists == 1)
            {

                SqlCommand updatePersonCmd = new SqlCommand("UPDATE Person SET FirstName = @FirstName, LastName = @LastName, " +
                                                           "Contact = @Contact, Email = @Email, DateOfBirth = @DateOfBirth, Gender = @Gender " +
                                                           "WHERE Id = @Id", con);

                updatePersonCmd.Parameters.AddWithValue("@Id", Id);
                updatePersonCmd.Parameters.AddWithValue("@FirstName", firstname.Text);
                updatePersonCmd.Parameters.AddWithValue("@LastName", lastname.Text);
                updatePersonCmd.Parameters.AddWithValue("@Contact", contact.Text);
                updatePersonCmd.Parameters.AddWithValue("@Email", email.Text);
                updatePersonCmd.Parameters.AddWithValue("@DateOfBirth", Convert.ToDateTime(birth.Text));
                updatePersonCmd.Parameters.AddWithValue("@Gender", int.Parse(gender.Text));

                updatePersonCmd.ExecuteNonQuery();

                SqlCommand updateStudentCmd = new SqlCommand("UPDATE Student SET RegistrationNo = @RegistrationNo WHERE Id = @Id", con);
                updateStudentCmd.Parameters.AddWithValue("@Id",Id);
                updateStudentCmd.Parameters.AddWithValue("@RegistrationNo", Regno.Text);

                updateStudentCmd.ExecuteNonQuery();

                MessageBox.Show("Successfully updated Student data");
            }
            else
            {
                MessageBox.Show("Student with the provided Id does not exist");
            }

        }
        private int AddOrUpdatePersonAndGetId(string firstName, string lastName, string contact, string email, DateTime dateOfBirth, int genderId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand insertCmd = new SqlCommand("INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) " +
                                                  "VALUES (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender); " +
                                                  "SELECT SCOPE_IDENTITY();", con);

            insertCmd.Parameters.AddWithValue("@FirstName", firstName);
            insertCmd.Parameters.AddWithValue("@LastName", lastName);
            insertCmd.Parameters.AddWithValue("@Contact", contact);
            insertCmd.Parameters.AddWithValue("@Email", email);
            insertCmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth);
            insertCmd.Parameters.AddWithValue("@Gender", genderId);


            return Convert.ToInt32(insertCmd.ExecuteScalar());
        }



        void AddStudent(object sender, EventArgs e)
        {
            int personId = AddOrUpdatePersonAndGetId(firstname.Text, lastname.Text, contact.Text, email.Text, Convert.ToDateTime(birth.Text), Convert.ToInt32(gender.Text));
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Student VALUES (@Id, @RegistrationNo)", con);
            cmd.Parameters.AddWithValue("@Id", personId);
            cmd.Parameters.AddWithValue("@RegistrationNo", Regno.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved into Student");
           IDLabel.Text = $"Your ID: {personId}";
            IDLabel.ForeColor = System.Drawing.Color.Red;
        }

        private void IDLabel_Click(object sender, EventArgs e)
        {

        }



       

       

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lastname_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
