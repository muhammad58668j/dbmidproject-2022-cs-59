﻿namespace mid_project
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.create = new Guna.UI2.WinForms.Guna2Button();
            this.advisorchkbx = new System.Windows.Forms.CheckBox();
            this.projectchkbx = new System.Windows.Forms.CheckBox();
            this.evaluationchkbx = new System.Windows.Forms.CheckBox();
            this.groupchkbx = new System.Windows.Forms.CheckBox();
            this.studentchkbx = new System.Windows.Forms.CheckBox();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.3467F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.6533F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.crystalReportViewer1, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 1);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1319, 662);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.18565F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.81435F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(354, 656);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.studentchkbx, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.groupchkbx, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.evaluationchkbx, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.projectchkbx, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.create, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.advisorchkbx, 0, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(348, 421);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // create
            // 
            this.create.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.create.BorderRadius = 15;
            this.create.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.create.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.create.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.create.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.create.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.create.ForeColor = System.Drawing.Color.White;
            this.create.Location = new System.Drawing.Point(30, 5);
            this.create.Margin = new System.Windows.Forms.Padding(30, 5, 20, 5);
            this.create.Name = "create";
            this.create.Size = new System.Drawing.Size(298, 60);
            this.create.TabIndex = 0;
            this.create.Text = "Create report";
            this.create.Click += new System.EventHandler(this.create_Click);
            // 
            // advisorchkbx
            // 
            this.advisorchkbx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.advisorchkbx.AutoSize = true;
            this.advisorchkbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advisorchkbx.Location = new System.Drawing.Point(3, 143);
            this.advisorchkbx.Name = "advisorchkbx";
            this.advisorchkbx.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.advisorchkbx.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.advisorchkbx.Size = new System.Drawing.Size(342, 64);
            this.advisorchkbx.TabIndex = 2;
            this.advisorchkbx.Text = "Advisor Report";
            this.advisorchkbx.UseVisualStyleBackColor = true;
            // 
            // projectchkbx
            // 
            this.projectchkbx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projectchkbx.AutoSize = true;
            this.projectchkbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projectchkbx.Location = new System.Drawing.Point(3, 213);
            this.projectchkbx.Name = "projectchkbx";
            this.projectchkbx.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.projectchkbx.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.projectchkbx.Size = new System.Drawing.Size(342, 64);
            this.projectchkbx.TabIndex = 3;
            this.projectchkbx.Text = "Project Report";
            this.projectchkbx.UseVisualStyleBackColor = true;
            // 
            // evaluationchkbx
            // 
            this.evaluationchkbx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.evaluationchkbx.AutoSize = true;
            this.evaluationchkbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationchkbx.Location = new System.Drawing.Point(3, 283);
            this.evaluationchkbx.Name = "evaluationchkbx";
            this.evaluationchkbx.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.evaluationchkbx.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.evaluationchkbx.Size = new System.Drawing.Size(342, 64);
            this.evaluationchkbx.TabIndex = 4;
            this.evaluationchkbx.Text = "Evaluation Report";
            this.evaluationchkbx.UseVisualStyleBackColor = true;
            // 
            // groupchkbx
            // 
            this.groupchkbx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupchkbx.AutoSize = true;
            this.groupchkbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupchkbx.Location = new System.Drawing.Point(3, 353);
            this.groupchkbx.Name = "groupchkbx";
            this.groupchkbx.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.groupchkbx.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupchkbx.Size = new System.Drawing.Size(342, 65);
            this.groupchkbx.TabIndex = 5;
            this.groupchkbx.Text = "Group Report";
            this.groupchkbx.UseVisualStyleBackColor = true;
            // 
            // studentchkbx
            // 
            this.studentchkbx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentchkbx.AutoSize = true;
            this.studentchkbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentchkbx.Location = new System.Drawing.Point(3, 73);
            this.studentchkbx.Name = "studentchkbx";
            this.studentchkbx.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.studentchkbx.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.studentchkbx.Size = new System.Drawing.Size(342, 64);
            this.studentchkbx.TabIndex = 6;
            this.studentchkbx.Text = "Student Report";
            this.studentchkbx.UseVisualStyleBackColor = true;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Location = new System.Drawing.Point(363, 3);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(953, 656);
            this.crystalReportViewer1.TabIndex = 1;
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1326, 665);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Report";
            this.Text = "Report";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Guna.UI2.WinForms.Guna2Button create;
        private System.Windows.Forms.CheckBox advisorchkbx;
        private System.Windows.Forms.CheckBox studentchkbx;
        private System.Windows.Forms.CheckBox groupchkbx;
        private System.Windows.Forms.CheckBox evaluationchkbx;
        private System.Windows.Forms.CheckBox projectchkbx;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
    }
}