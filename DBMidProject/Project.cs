﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace mid_project
{
    public partial class Project : Form
    {
        public Project()
        {
            InitializeComponent();
            view();

        }
        public static DataTable GetProjectTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select id,title AS Title,Description from Project", con);
            DataTable dt = new DataTable();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            
            return dt;
        }
        public static DataTable GetProjectWithAdvisorsNames(string str = null)
        {
            var con = Configuration.getInstance().getConnection();
            
            using (DataTable dt = new DataTable("Person"))
            {
                using (SqlCommand cmd = new SqlCommand(" select GroupId,Project.Title ,GP.AssignmentDate ,(select Person.FirstName+' '+Person.LastName from ProjectAdvisor PA join Person on Person.Id= Pa.AdvisorId where GP.ProjectId = Pa.ProjectId and PA.AdvisorRole = 11 ) as [Main Advisor] ,(select Person.FirstName+' '+Person.LastName from ProjectAdvisor PA join Person on Person.Id= Pa.AdvisorId where GP.ProjectId = Pa.ProjectId and PA.AdvisorRole = 12 ) as [Co Advisor] ,(select Person.FirstName+' '+Person.LastName from ProjectAdvisor PA join Person on Person.Id= Pa.AdvisorId where GP.ProjectId = Pa.ProjectId and PA.AdvisorRole = 14 ) as [Industry Advisor] from GroupProject GP join Project on Project.Id = GP.ProjectId where project.Title like @str_ ", con))
                {
                    cmd.Parameters.AddWithValue("str_", string.Format("%{0}%", str));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
                return dt;
            }
        }
        private void Back_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Close();
        }
        private void Add_Click(object sender, EventArgs e)
        {
           
            Form n = new AddProject(OperationMode.Add);
            n.Show();
            this.Close();

        }


        private void Update_Click(object sender, EventArgs e)
        {
            string mode = "updateproject";
            Form Idform = new IDFormcs(mode);
            Idform.Show();
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {

            string mod = "deleteproject";
            Form Idform = new IDFormcs(mod);
            Idform.Show();
            this.Close();

        }


        public void view()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * FROM Project WHERE Title NOT LIKE '@%' ", con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable studentDataTable = new DataTable();
            adapter.Fill(studentDataTable);
            dataGridView1.DataSource = studentDataTable;
        }
    }
}
