﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;
namespace mid_project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

   

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Student_Click(object sender, EventArgs e)
        {
            Form s = new student();
            s.Show();
        }

        private void Advisor_Click(object sender, EventArgs e)
        {
            
            Form A=new Advisor();
            A.Show(); 
        }

        private void project_Click(object sender, EventArgs e)
        {
            Form p = new Project();
            p.Show();
        }

        private void AssignAdvisor_Click(object sender, EventArgs e)
        {
            Form assign= new ProjectAdvisor();
            assign.Show();

        }

        private void Group_Click(object sender, EventArgs e)
        {
            Form f=new GroupStudent();
            f.Show();
        }

        private void AssignProject_Click(object sender, EventArgs e)
        {
            Form f = new AssignProject();
            f.Show();
        }

        private void Evaluation_Click(object sender, EventArgs e)
        {
            Form f=new Evaluation();
            f.Show();
        }

        private void EvaluationMarks_Click(object sender, EventArgs e)
        {
            Form f = new GroupEvaluation();
            f.Show();
        }

        private void reportbtn_Click(object sender, EventArgs e)
        {
            Form f = new Report();
            f.Show();
        }
    }
}
