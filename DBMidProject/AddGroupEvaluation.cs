﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class AddGroupEvaluation : Form
    {
        public AddGroupEvaluation()
        {
            InitializeComponent();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            addGroupEvaluation(sender, e);
        }
        void addGroupEvaluation(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO GroupEvaluation VALUES (@GroupId,@EvaluationId,@ObtainedMarks,@EvaluationDate)", con);
            cmd.Parameters.AddWithValue("@GroupId", groupid.Text);
            cmd.Parameters.AddWithValue("@EvaluationId", evaluationid.Text);
            cmd.Parameters.AddWithValue("@ObtainedMarks", Convert.ToInt32(obtainedmarks.Text));
            cmd.Parameters.AddWithValue("@EvaluationDate", DateTime.Now);

            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved into GroupEvaluation");

        }
    }
}
