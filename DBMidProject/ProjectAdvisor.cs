﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class ProjectAdvisor : Form
    {
        public ProjectAdvisor()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from ProjectAdvisor", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Close();
        }
        private void Add_Click(object sender, EventArgs e)
        {

            Form n = new AssignAdvisor();
            n.Show();
            this.Close();


        }


        private void Update_Click(object sender, EventArgs e)
        {
            string mode = "updateAssignAdvisor";
            Form Idform = new AssignedIDForm(mode);
            Idform.Show();
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {

            string mod = "deleteAssignAdvisor";
            Form Idform = new AssignedIDForm(mod);
            Idform.Show();
            this.Close();

        }
    }
}
