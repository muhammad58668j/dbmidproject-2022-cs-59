﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class IDFormcs : Form
    {
        private int Mode;
        public IDFormcs(int mode)
        {
            InitializeComponent();
            Mode = mode;
        }

        private void submit_Click(object sender, EventArgs e)
        {
            
        }

        private void submit_Click_1(object sender, EventArgs e)
        {
            int id = int.Parse(Idtxt.Text);
            if (Mode == 1)
            { 
                Form n = new AddForm(id, OperationMode.Update);
                n.Show();
                this.Close();
            }
            else if(Mode == 2)
            {
                delete(sender, e);
                this.Close();
            }
           
        }
        private void delete(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();


            SqlCommand checkStatusCmd = new SqlCommand("SELECT Status FROM GroupStudent WHERE StudentId = @StudentId", con);
            checkStatusCmd.Parameters.AddWithValue("@StudentId", int.Parse(Idtxt.Text));

            int currentStatus = (int)checkStatusCmd.ExecuteScalar();


            if (currentStatus == 3)
            {

                SqlCommand updateStatusCmd = new SqlCommand("UPDATE GroupStudent SET Status = 4 WHERE StudentId = @StudentId", con);
                updateStatusCmd.Parameters.AddWithValue("@StudentId", int.Parse(Idtxt.Text));
                int rowsAffected = updateStatusCmd.ExecuteNonQuery();

                if (rowsAffected > 0)
                {
                    MessageBox.Show("Successfully set status to Inactive in GroupStudent");
                }
                else
                {
                    MessageBox.Show("No rows were updated. Student may not exist in GroupStudent or already has Inactive status.");
                }
            }
            else
            {
                MessageBox.Show("Student already has Inactive status in GroupStudent. No further action needed.");
            }
        }
    }
}
