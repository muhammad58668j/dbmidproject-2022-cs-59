﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;

namespace mid_project
{
    public partial class student : Form
    {
        public student()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            string selectQuery = "SELECT S.Id,S.RegistrationNo,P.FirstName,P.LastName, P.Contact, P.Email, P.DateOfBirth, P.Gender FROM Person P JOIN Student S ON P.Id=S.Id ";
            SqlDataAdapter adapter = new SqlDataAdapter(selectQuery, con);
            DataTable studentDataTable = new DataTable();
            adapter.Fill(studentDataTable);
            dataGridView1.DataSource = studentDataTable;
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        

        private void Back_Click(object sender, EventArgs e)
        {
            Form b = new Form();
            b.Show();
        }



        private void Add_Click(object sender, EventArgs e)
        { 
            Form n= new AddForm(OperationMode.Add);
            n.Show();
            this.Close();

        }


        private void Update_Click(object sender, EventArgs e)
        {
            int mode = 1;
            Form Idform = new IDFormcs(mode);
              Idform.Show();
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {

            int mod = 2;
            Form Idform = new IDFormcs(mod);
            Idform.Show();
            this.Close();
          
        }

        private bool isFirstClick = true;

        private void Regno_GotFocus(object sender, EventArgs e)
        {
           
            if (isFirstClick)
            {
                Regno.Clear();
                isFirstClick = false;
            }
        }

        private void firstname_GotFocus(object sender, EventArgs e)
        {
            if (isFirstClick)
            {
                firstname.Clear();
                isFirstClick = false;
            }
        }

        private void lastname_GotFocus(object sender, EventArgs e)
        {
            if (isFirstClick)
            {
                lastname.Clear();
                isFirstClick = false;
            }
        }

        private void contact_GotFocus(object sender, EventArgs e)
        {
            if (isFirstClick)
            {
                contact.Clear();
                isFirstClick = false;
            }
        }

        private void email_GotFocus(object sender, EventArgs e)
        {
            if (isFirstClick)
            {
                email.Clear();
                isFirstClick = false;
            }
        }

        private void birth_GotFocus(object sender, EventArgs e)
        {
            if (isFirstClick)
            {
                birth.Clear();
                isFirstClick = false;
            }
        }

        private void gender_GotFocus(object sender, EventArgs e)
        {
            if (isFirstClick)
            {
                gender.Clear();
                isFirstClick = false;
            }
        }

        private void idl_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
