﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace mid_project
{
   
    public partial class AddAdvisor : Form
    {
        private OperationMode Mode;
        private int Id;

        public AddAdvisor(int id, OperationMode mode)
        {
            InitializeComponent();
            Id = id;
            Mode = mode;
            IDLabel.Text = $"Your ID: {Id}";
            IDLabel.ForeColor = System.Drawing.Color.Red;

        }

        public AddAdvisor(OperationMode mode)
        {
            InitializeComponent();
            Mode = mode;
          
        }
       

        private void submit_Click(object sender, EventArgs e)
        {
           
                if (Mode == OperationMode.Add)
                {
                    addAdvisor(sender, e);
                }
                else if (Mode == OperationMode.Update)
                {
                    updateAdvisor(sender, e);
                }
          
            
        }


        private void updateAdvisor(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand checkAdvisorCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Advisor WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkAdvisorCmd.Parameters.AddWithValue("@Id", Id);

            int studentExists = (int)checkAdvisorCmd.ExecuteScalar();

            if (studentExists == 1)
            {
                SqlCommand updatePersonCmd = new SqlCommand("UPDATE Person SET FirstName = @FirstName, LastName = @LastName, " +
                                                          "Contact = @Contact, Email = @Email, DateOfBirth = @DateOfBirth, Gender = @Gender " +
                                                          "WHERE Id = @Id", con);

                updatePersonCmd.Parameters.AddWithValue("@Id", Id);
                updatePersonCmd.Parameters.AddWithValue("@FirstName", firstname.Text);
                updatePersonCmd.Parameters.AddWithValue("@LastName", lastname.Text);
                updatePersonCmd.Parameters.AddWithValue("@Contact", contact.Text);
                updatePersonCmd.Parameters.AddWithValue("@Email", email.Text);
                updatePersonCmd.Parameters.AddWithValue("@DateOfBirth", Convert.ToDateTime(birth.Text));
                updatePersonCmd.Parameters.AddWithValue("@Gender", int.Parse(gender.Text));
                updatePersonCmd.ExecuteNonQuery();

                SqlCommand updateAdvisorCmd = new SqlCommand("UPDATE Advisor SET Designation = @Designation ,Salary=@Salary WHERE Id = @Id", con);
                updateAdvisorCmd.Parameters.AddWithValue("@Id", Id);
                updateAdvisorCmd.Parameters.AddWithValue("@Designation", Designation.Text);
                updateAdvisorCmd.Parameters.AddWithValue("@Salary", Salary.Text);

                updateAdvisorCmd.ExecuteNonQuery();

                MessageBox.Show("Successfully updated Advisor data");
            }
            else
            {
                MessageBox.Show("Advisor with the provided Id does not exist");
            }

        }

        private int AddOrUpdatePersonAndGetId(string firstName, string lastName, string contact, string email, DateTime dateOfBirth, int genderId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand insertCmd = new SqlCommand("INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) " +
                                                  "VALUES (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender); " +
                                                  "SELECT SCOPE_IDENTITY();", con);

            insertCmd.Parameters.AddWithValue("@FirstName", firstName);
            insertCmd.Parameters.AddWithValue("@LastName", lastName);
            insertCmd.Parameters.AddWithValue("@Contact", contact);
            insertCmd.Parameters.AddWithValue("@Email", email);
            insertCmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth);
            insertCmd.Parameters.AddWithValue("@Gender", genderId);


            return Convert.ToInt32(insertCmd.ExecuteScalar());
        }
        void addAdvisor(object sender, EventArgs e)
        {
            int personId = AddOrUpdatePersonAndGetId(firstname.Text, lastname.Text, contact.Text, email.Text, Convert.ToDateTime(birth.Text), Convert.ToInt32(gender.Text));
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Advisor VALUES (@Id, @Designation,@Salary)", con);
            cmd.Parameters.AddWithValue("@Id", personId);
            cmd.Parameters.AddWithValue("@Designation", Designation.Text);
            cmd.Parameters.AddWithValue("@Salary", Salary.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved into Advisor");
            IDLabel.Text = $"Your ID: {personId}";
            IDLabel.ForeColor = System.Drawing.Color.Red;
        }

      


        private void IDLabel_Click(object sender, EventArgs e)
        {

        }

       
    }
}
