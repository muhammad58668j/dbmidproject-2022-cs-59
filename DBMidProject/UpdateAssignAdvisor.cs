﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class UpdateAssignAdvisor : Form
    {
        private int advisorid;
        private int projectid;
        public UpdateAssignAdvisor(int advisorid, int projectid)
        {
            InitializeComponent();
            this.advisorid = advisorid;
            this.projectid = projectid;
            IDLabel.Text = $"AdvisorId:{advisorid}+ And PId:{projectid} ";
            IDLabel.ForeColor = System.Drawing.Color.Red;
        }

        private void submit_Click(object sender, EventArgs e)
        {
            
            updateadvisor(sender, e);
        }
        private void updateadvisor(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkAdvisorCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM ProjectAdvisor WHERE AdvisorId = @AdvisorId) SELECT 1 ELSE SELECT 0", con);
            checkAdvisorCmd.Parameters.AddWithValue("@AdvisorId", advisorid);
            int AdvisorExists = (int)checkAdvisorCmd.ExecuteScalar();

            SqlCommand checkProjectCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM ProjectAdvisor WHERE ProjectId = @ProjectId) SELECT 1 ELSE SELECT 0", con);
            checkProjectCmd.Parameters.AddWithValue("@ProjectId", advisorid);

            int ProjectExists = (int)checkAdvisorCmd.ExecuteScalar();

            if (AdvisorExists == 1 && ProjectExists == 1)
            {
                SqlCommand cmd = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorRole=@AdvisorRole,AssignmentDate=@AssignmentDate  WHERE AdvisorId = @AdvisorId AND ProjectId = @ProjectId", con);
                cmd.Parameters.AddWithValue("@AdvisorId", advisorid.ToString());
                cmd.Parameters.AddWithValue("@ProjectId", projectid.ToString());
                cmd.Parameters.AddWithValue("@AdvisorRole", advisorRole.Text);
                cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated into ProjectAdvisor");
            }
            else
            {
                MessageBox.Show("Please Enter the Valid ID");
            }
        }

        private void UpdateAssignAdvisor_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
