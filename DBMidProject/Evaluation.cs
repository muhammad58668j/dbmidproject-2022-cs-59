﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace mid_project
{
    public partial class Evaluation : Form
    {
        public Evaluation()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * FROM Evaluation WHERE Name NOT LIKE '@%'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        public static DataTable GetEvaluations(string str = "")
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Evaluation where name like @str", con);
            cmd.Parameters.AddWithValue("str", string.Format("%{0}%", str));
            DataTable dt = new DataTable();
          
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            
            return dt;
        }
        public static DataTable GetEvaluationFromGid(int Gid)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select E.Id,GE.GroupId, E.Name, GE.ObtainedMarks, E.TotalMarks, E.TotalWeightage,GE.EvaluationDate from Evaluation E join GroupEvaluation GE on E.Id = GE.EvaluationId where GE.GroupId = @Gid", con);
            cmd.Parameters.AddWithValue("Gid", Gid);
            DataTable dt = new DataTable();
            
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
             
            return dt;
        }
        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Back_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Close();
        }
        private void Add_Click(object sender, EventArgs e)
        {

            Form n = new AddEvaluation(OperationMode.Add);
            n.Show();
            this.Close();


        }


        private void Update_Click(object sender, EventArgs e)
        {
            string mode = "updateEvaluation";
            Form Idform = new IDFormcs(mode);
            Idform.Show();
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {

            string mod = "deleteEvaluation";
            Form Idform = new IDFormcs(mod);
            Idform.Show();
            this.Close();

        }
    }
}
