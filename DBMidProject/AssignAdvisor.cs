﻿using Guna.UI2.WinForms.Suite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace mid_project
{
    public partial class AssignAdvisor : Form
    {
        
      
        public AssignAdvisor()
        {
            InitializeComponent();
        }
        private void submit_Click(object sender, EventArgs e)
        {
                asignadvisor(sender, e);  
        }
        private void asignadvisor(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkAdvisorCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Advisor WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkAdvisorCmd.Parameters.AddWithValue("@Id", advisorId.Text);
            int AdvisorExists = (int)checkAdvisorCmd.ExecuteScalar();

            SqlCommand checkProjectCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Project WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkProjectCmd.Parameters.AddWithValue("@Id", advisorId.Text);

            int ProjectExists = (int)checkAdvisorCmd.ExecuteScalar();

            if (AdvisorExists == 1 && ProjectExists==1)
            { 
                SqlCommand cmd = new SqlCommand("INSERT INTO ProjectAdvisor VALUES (@AdvisorId,@ProjectId,@AdvisorRole,@AssignmentDate);", con);
                cmd.Parameters.AddWithValue("@AdvisorId", advisorId.Text);
                cmd.Parameters.AddWithValue("@ProjectId", projectId.Text);
                cmd.Parameters.AddWithValue("@AdvisorRole", advisorRole.Text);
                cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Assigned into ProjectAdvisor");
            }
            else
            {
                MessageBox.Show("Please Enter the Valid ID");
            }
        }

        private void advisorId_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
