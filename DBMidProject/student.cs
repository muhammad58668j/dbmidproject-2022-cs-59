﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Org.BouncyCastle.Math.EC.ECCurve;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;

namespace mid_project
{
    public partial class student : Form
    {
        public student()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            string selectQuery = "SELECT S.Id,S.RegistrationNo,P.FirstName,P.LastName, P.Contact, P.Email, P.DateOfBirth, P.Gender FROM Person P JOIN Student S ON P.Id=S.Id ";
            SqlDataAdapter adapter = new SqlDataAdapter(selectQuery, con);
            DataTable studentDataTable = new DataTable();
            adapter.Fill(studentDataTable);
            dataGridView1.DataSource = studentDataTable;
        }
        public static DataTable GetStudentTableDetails()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.id,p.FirstName,p.LastName,s.RegistrationNo,p.Contact,p.Email,p.DateOfBirth,l.Value as Gender " +
                "from student as s " +
                "join person as p on s.id = p.id " +
                "join lookup as l on p.gender = l.id", con);
            DataTable dt = new DataTable();
            
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            
            return dt;
        }
        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        

        private void Back_Click(object sender, EventArgs e)
        {
            Form b = new Form();
            b.Show();
        }



        private void Add_Click(object sender, EventArgs e)
        { 
            Form n= new AddForm(OperationMode.Add);
            n.Show();
            this.Close();

        }


        private void Update_Click(object sender, EventArgs e)
        {
            string mode = "updatestudent";
            Form Idform = new IDFormcs(mode);
              Idform.Show();
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {

            string mod = "deletestudent";
            Form Idform = new IDFormcs(mod);
            Idform.Show();
            this.Close();
          
        }


        private void idl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

      
    }
}
