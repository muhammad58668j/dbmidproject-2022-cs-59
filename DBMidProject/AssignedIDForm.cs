﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class AssignedIDForm : Form
    {
        private string Mode;
        private int integ;
        private Double intege;
        private Char At;
        bool type;
        public AssignedIDForm(string mode)
        {
            InitializeComponent();
            Mode = mode;
        }
        public AssignedIDForm(string mode,int i)
        {
            InitializeComponent();
            Mode = mode;
            integ = i;
            
            AdvisorId.Text = "Enter Group ID";
            ProjectId.Text = "Enter Student ID";
        }
        public AssignedIDForm(string mode, Double f)
        {
            InitializeComponent();
            Mode = mode;
            intege = f;
            AdvisorId.Text = "Enter Project ID";
            ProjectId.Text = "Enter Group ID ";
        }
        public AssignedIDForm(string mode, Char at)
        {
            InitializeComponent();
            Mode = mode;
            At = at;
            AdvisorId.Text = "Enter New Project ID";
            ProjectId.Text = "Enter Group ID ";
        }
        public AssignedIDForm(string mode, bool typ)
        {
            InitializeComponent();
            Mode = mode;
            type = typ;
            AdvisorId.Text = "Enter Group ID";
            ProjectId.Text = "Enter Evaluation ID ";
        }


        private void submit_Click(object sender, EventArgs e)
        {
            int id = int.Parse(AdvisorId.Text);
            int pid=int.Parse(ProjectId.Text);
            if (Mode == "updateAssignAdvisor")
            {
                Form n = new UpdateAssignAdvisor(id,pid);
                n.Show();
                this.Close();
            }
            else if (Mode == "deleteAssignAdvisor")
            {
                deleteAssignAdvisor(sender, e);
                this.Close();
            }
            else if(Mode =="deleteGroup")
            {
                deletegroup(sender,e);
            }
            else if(Mode=="AddAssignProject")
            {
                assignproject(sender, e);
            }
            else if (Mode == "updateAssignProject")
            {
                assignproject(sender, e);
            }
            if (Mode == "updateEvaluation")
            {
                Form n = new UpdateGroupEvaluation(id, pid);
                n.Show();
                this.Close();
            }
            else if (Mode == "deleteEvaluation")
            {
                deleteGroupEvaluation(sender, e);
                this.Close();
            }

        }

    
        private void deleteAssignAdvisor(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkAdvisorCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM ProjectAdvisor WHERE AdvisorId = @AdvisorId AND ProjectId=@ProjectId) SELECT 1 ELSE SELECT 0", con);
            checkAdvisorCmd.Parameters.AddWithValue("@AdvisorId", int.Parse(AdvisorId.Text));
            checkAdvisorCmd.Parameters.AddWithValue("@ProjectId", int.Parse(ProjectId.Text));

            int AdvisorExists = (int)checkAdvisorCmd.ExecuteScalar();

            if (AdvisorExists == 1)
            {
                SqlCommand checkStatusCmd = new SqlCommand("SELECT AssignmentDate FROM ProjectAdvisor WHERE AdvisorId = @AdvisorId AND ProjectId=@ProjectId", con);
                checkStatusCmd.Parameters.AddWithValue("@AdvisorId", int.Parse(AdvisorId.Text));
                checkStatusCmd.Parameters.AddWithValue("@ProjectId", int.Parse(ProjectId.Text));

                DateTime result = (DateTime)checkStatusCmd.ExecuteScalar();


                SqlCommand updateAdvisorCmd = new SqlCommand("UPDATE ProjectAdvisor SET AssignmentDate=@AssignmentDate WHERE AdvisorId = @AdvisorId AND ProjectId=@ProjectId", con);
                updateAdvisorCmd.Parameters.AddWithValue("@AdvisorId", int.Parse(AdvisorId.Text));
                updateAdvisorCmd.Parameters.AddWithValue("@ProjectId", int.Parse(ProjectId.Text));
                updateAdvisorCmd.Parameters.AddWithValue("@AssignmentDate",$"#{result}");



                updateAdvisorCmd.ExecuteNonQuery();

                MessageBox.Show("Successfully updated AssignmentDate");
               
            }
            else
            {
                MessageBox.Show("Provided Id does not exist");
            }


        }
       private void deletegroup(object sender,EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkGroupCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM GroupStudent WHERE GroupId = @GroupId AND StudentId = @StudentId) SELECT 1 ELSE SELECT 0", con);
            checkGroupCmd.Parameters.AddWithValue("@GroupId", AdvisorId.Text);
            checkGroupCmd.Parameters.AddWithValue("@StudentId", ProjectId.Text);
            int GroupExists = (int)checkGroupCmd.ExecuteScalar();

            if (GroupExists == 1 )
            {

                SqlCommand GroupCmd = new SqlCommand("UPDATE GroupStudent SET Status = @Status WHERE GroupId=@GroupId AND StudentId = @StudentId ", con);
                 GroupCmd.Parameters.AddWithValue("@GroupId", AdvisorId.Text);
                GroupCmd.Parameters.AddWithValue("@StudentId", ProjectId.Text);
                 GroupCmd.Parameters.AddWithValue("@Status", 4); 
            
                 GroupCmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Deleted group for the student.");
                Form f = new AddGroup();
                f.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Please enter valid student and group IDs.");
            }

        }
        private void deleteGroupEvaluation(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkGroupCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM GroupEvaluation WHERE GroupId = @GroupId AND EvaluationId = @EvaluationId) SELECT 1 ELSE SELECT 0", con);
            checkGroupCmd.Parameters.AddWithValue("@GroupId", AdvisorId.Text);
            checkGroupCmd.Parameters.AddWithValue("@EvaluationId", ProjectId.Text);
            int GroupExists = (int)checkGroupCmd.ExecuteScalar();

            if (GroupExists == 1)
            {
                SqlCommand checkStatusCmd = new SqlCommand("SELECT ObtainedMarks FROM GroupEvaluation WHERE GroupId=@GroupId AND EvaluationId = @EvaluationId ", con);
                checkStatusCmd.Parameters.AddWithValue("@GroupId", int.Parse(AdvisorId.Text));
                checkStatusCmd.Parameters.AddWithValue("@EvaluationId", int.Parse(ProjectId.Text));

                object result = checkStatusCmd.ExecuteScalar();
                int marks = -(Convert.ToInt32(result)) + 0;


                SqlCommand GroupCmd = new SqlCommand("UPDATE GroupEvaluation SET ObtainedMarks=@ObtainedMarks WHERE GroupId=@GroupId AND EvaluationId = @EvaluationId ", con);
                GroupCmd.Parameters.AddWithValue("@GroupId", int.Parse(AdvisorId.Text));
                GroupCmd.Parameters.AddWithValue("@EvaluationId",int.Parse( ProjectId.Text));
                GroupCmd.Parameters.AddWithValue("@ObtainedMarks", marks);

                GroupCmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Deleted group for the GroupEvaluation.");
       
            }
            else
            {
                MessageBox.Show("Please enter valid group IDs.");
            }

        }
        private void assignproject(object sender,EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand InsertProjectCmd = new SqlCommand("INSERT INTO GroupProject (ProjectId,GroupId,AssignmentDate) Values(@ProjectId,@GroupId,@AssignmentDate)", con);
            InsertProjectCmd.Parameters.AddWithValue("@ProjectId", AdvisorId.Text);
            InsertProjectCmd.Parameters.AddWithValue("@GroupId", ProjectId.Text);
            InsertProjectCmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
            InsertProjectCmd.ExecuteNonQuery();
            MessageBox.Show(" project is Assigned Succesfully");
            
        }

    }
}
