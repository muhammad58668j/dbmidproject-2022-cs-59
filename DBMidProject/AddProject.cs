﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project

{
    public partial class AddProject : Form
    {
        private OperationMode Mode;
        private int Id;

        public AddProject(int id, OperationMode mode)
        {
            InitializeComponent();
            Id = id;
            Mode = mode;
            IDLabel.Text = $"Your ID: {Id}";
            IDLabel.ForeColor = System.Drawing.Color.Red;

        }
        public AddProject( OperationMode mode)
        {
            InitializeComponent();
 
            Mode = mode;
           

        }
        private void submit_Click(object sender, EventArgs e)
        {
            if (Mode == OperationMode.Update)
            {
                updateproject(sender, e);
            }
            else if (Mode == OperationMode.Add)
            {
                addproject(sender, e);
            }
        }
        void addproject(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Project VALUES (@Description,@Title); SELECT SCOPE_IDENTITY();", con);
            cmd.Parameters.AddWithValue("@Description", description.Text);
            cmd.Parameters.AddWithValue("@Title", title.Text);
            int id= Convert.ToInt32(cmd.ExecuteScalar());
            MessageBox.Show("Successfully saved into Project");
            IDLabel.Text = $"Your Project ID: {id}";
            IDLabel.ForeColor = System.Drawing.Color.Red;
        }

        private void updateproject(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand checkStudentCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Project WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkStudentCmd.Parameters.AddWithValue("@Id", Id);

            int studentExists = (int)checkStudentCmd.ExecuteScalar();

            if (studentExists == 1)
            {


                SqlCommand Cmd = new SqlCommand("UPDATE Project SET Description = @Description ,Title=@Title WHERE Id = @Id", con);
                Cmd.Parameters.AddWithValue("@Id", Id);
                Cmd.Parameters.AddWithValue("@Description", description.Text);
                Cmd.Parameters.AddWithValue("@Title", title.Text);

                Cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully updated Project data");
            }
            else
            {
                MessageBox.Show("Project with the provided Id does not exist");
            }

        }

        private void description_TextChanged(object sender, EventArgs e)
        {

        }

        private void title_TextChanged(object sender, EventArgs e)
        {

        }

        private void IDLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
