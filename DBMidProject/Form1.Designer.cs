﻿namespace mid_project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.reportbtn = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.AssignProject = new System.Windows.Forms.Button();
            this.AssignAdvisor = new System.Windows.Forms.Button();
            this.Evaluation = new System.Windows.Forms.Button();
            this.EvaluationMarks = new System.Windows.Forms.Button();
            this.Student = new System.Windows.Forms.Button();
            this.Avisor = new System.Windows.Forms.Button();
            this.project = new System.Windows.Forms.Button();
            this.Group = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1185F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.73309F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.26691F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1185, 547);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.45621F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.53796F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.005839F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.reportbtn, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1179, 91);
            this.tableLayoutPanel2.TabIndex = 0;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(244, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(825, 91);
            this.label1.TabIndex = 0;
            this.label1.Text = "FYP Management System";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // reportbtn
            // 
            this.reportbtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportbtn.BorderColor = System.Drawing.Color.Maroon;
            this.reportbtn.BorderRadius = 10;
            this.reportbtn.BorderThickness = 3;
            this.reportbtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.reportbtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.reportbtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.reportbtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.reportbtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.reportbtn.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportbtn.ForeColor = System.Drawing.Color.White;
            this.reportbtn.Location = new System.Drawing.Point(3, 43);
            this.reportbtn.Name = "reportbtn";
            this.reportbtn.Size = new System.Drawing.Size(235, 45);
            this.reportbtn.TabIndex = 1;
            this.reportbtn.Text = "Generate Report";
            this.reportbtn.Click += new System.EventHandler(this.reportbtn_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.DarkGray;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.AssignProject, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.AssignAdvisor, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.Evaluation, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.EvaluationMarks, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Student, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.Avisor, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.project, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.Group, 0, 0);
            this.tableLayoutPanel3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 100);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(40, 0, 40, 60);
            this.tableLayoutPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1179, 444);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // AssignProject
            // 
            this.AssignProject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AssignProject.BackColor = System.Drawing.Color.Gray;
            this.AssignProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssignProject.Image = global::mid_project.Properties.Resources.icons8_batch_assign_100;
            this.AssignProject.Location = new System.Drawing.Point(60, 212);
            this.AssignProject.Margin = new System.Windows.Forms.Padding(20);
            this.AssignProject.Name = "AssignProject";
            this.AssignProject.Size = new System.Drawing.Size(237, 152);
            this.AssignProject.TabIndex = 7;
            this.AssignProject.Text = "Assigned Projects";
            this.AssignProject.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AssignProject.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AssignProject.UseVisualStyleBackColor = false;
            this.AssignProject.Click += new System.EventHandler(this.AssignProject_Click);
            // 
            // AssignAdvisor
            // 
            this.AssignAdvisor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AssignAdvisor.BackColor = System.Drawing.Color.Gray;
            this.AssignAdvisor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssignAdvisor.Image = global::mid_project.Properties.Resources.icons8_consultation_100;
            this.AssignAdvisor.Location = new System.Drawing.Point(337, 212);
            this.AssignAdvisor.Margin = new System.Windows.Forms.Padding(20);
            this.AssignAdvisor.Name = "AssignAdvisor";
            this.AssignAdvisor.Size = new System.Drawing.Size(234, 152);
            this.AssignAdvisor.TabIndex = 6;
            this.AssignAdvisor.Text = "Assign  Advisor";
            this.AssignAdvisor.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AssignAdvisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AssignAdvisor.UseVisualStyleBackColor = false;
            this.AssignAdvisor.Click += new System.EventHandler(this.AssignAdvisor_Click);
            // 
            // Evaluation
            // 
            this.Evaluation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Evaluation.BackColor = System.Drawing.Color.Gray;
            this.Evaluation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Evaluation.Image = global::mid_project.Properties.Resources.icons8_evaluation_100;
            this.Evaluation.Location = new System.Drawing.Point(611, 212);
            this.Evaluation.Margin = new System.Windows.Forms.Padding(20);
            this.Evaluation.Name = "Evaluation";
            this.Evaluation.Size = new System.Drawing.Size(234, 152);
            this.Evaluation.TabIndex = 5;
            this.Evaluation.Text = "Evaluation";
            this.Evaluation.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Evaluation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Evaluation.UseVisualStyleBackColor = false;
            this.Evaluation.Click += new System.EventHandler(this.Evaluation_Click);
            // 
            // EvaluationMarks
            // 
            this.EvaluationMarks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EvaluationMarks.BackColor = System.Drawing.Color.Gray;
            this.EvaluationMarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvaluationMarks.Image = global::mid_project.Properties.Resources.icons8_exam_100;
            this.EvaluationMarks.Location = new System.Drawing.Point(885, 212);
            this.EvaluationMarks.Margin = new System.Windows.Forms.Padding(20);
            this.EvaluationMarks.Name = "EvaluationMarks";
            this.EvaluationMarks.Size = new System.Drawing.Size(234, 152);
            this.EvaluationMarks.TabIndex = 4;
            this.EvaluationMarks.Text = "Evaluation Marks";
            this.EvaluationMarks.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.EvaluationMarks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.EvaluationMarks.UseVisualStyleBackColor = false;
            this.EvaluationMarks.Click += new System.EventHandler(this.EvaluationMarks_Click);
            // 
            // Student
            // 
            this.Student.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Student.BackColor = System.Drawing.Color.Gray;
            this.Student.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Student.Image = ((System.Drawing.Image)(resources.GetObject("Student.Image")));
            this.Student.Location = new System.Drawing.Point(60, 20);
            this.Student.Margin = new System.Windows.Forms.Padding(20);
            this.Student.Name = "Student";
            this.Student.Size = new System.Drawing.Size(237, 152);
            this.Student.TabIndex = 3;
            this.Student.Text = "Students";
            this.Student.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Student.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Student.UseVisualStyleBackColor = false;
            this.Student.Click += new System.EventHandler(this.Student_Click);
            // 
            // Avisor
            // 
            this.Avisor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Avisor.BackColor = System.Drawing.Color.Gray;
            this.Avisor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Avisor.Image = global::mid_project.Properties.Resources.advisor;
            this.Avisor.Location = new System.Drawing.Point(337, 20);
            this.Avisor.Margin = new System.Windows.Forms.Padding(20);
            this.Avisor.Name = "Avisor";
            this.Avisor.Size = new System.Drawing.Size(234, 152);
            this.Avisor.TabIndex = 2;
            this.Avisor.Text = "Advisors";
            this.Avisor.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Avisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Avisor.UseVisualStyleBackColor = false;
            this.Avisor.Click += new System.EventHandler(this.Advisor_Click);
            // 
            // project
            // 
            this.project.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.project.BackColor = System.Drawing.Color.Gray;
            this.project.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.project.Image = global::mid_project.Properties.Resources.project;
            this.project.Location = new System.Drawing.Point(611, 20);
            this.project.Margin = new System.Windows.Forms.Padding(20);
            this.project.Name = "project";
            this.project.Size = new System.Drawing.Size(234, 152);
            this.project.TabIndex = 1;
            this.project.Text = "Projects";
            this.project.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.project.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.project.UseVisualStyleBackColor = false;
            this.project.Click += new System.EventHandler(this.project_Click);
            // 
            // Group
            // 
            this.Group.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Group.BackColor = System.Drawing.Color.Gray;
            this.Group.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Group.Image = global::mid_project.Properties.Resources.student_group;
            this.Group.Location = new System.Drawing.Point(885, 20);
            this.Group.Margin = new System.Windows.Forms.Padding(20);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(234, 152);
            this.Group.TabIndex = 0;
            this.Group.Text = "Students Groups";
            this.Group.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Group.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Group.UseVisualStyleBackColor = false;
            this.Group.Click += new System.EventHandler(this.Group_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(1209, 571);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button Group;
        private System.Windows.Forms.Button AssignProject;
        private System.Windows.Forms.Button AssignAdvisor;
        private System.Windows.Forms.Button Evaluation;
        private System.Windows.Forms.Button EvaluationMarks;
        private System.Windows.Forms.Button Student;
        private System.Windows.Forms.Button Avisor;
        private System.Windows.Forms.Button project;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button reportbtn;
    }
}

