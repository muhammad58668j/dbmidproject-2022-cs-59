﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mid_project
{
    public partial class IDFormcs : Form
    {
        private string Mode;
        public IDFormcs(string mode)
        {
            InitializeComponent();
            Mode = mode;
        }

      

        private void submit_Click_1(object sender, EventArgs e)
        {
            int id = int.Parse(Idtxt.Text);
            if (Mode == "updatestudent")
            { 
                Form n = new AddForm(id, OperationMode.Update);
                n.Show();
                this.Close();
            }
            else if(Mode == "deletestudent")
            {
                deleteStudent(sender, e);
                this.Close();
            }
            else if(Mode =="updateadvisor")
            {
                Form f = new AddAdvisor(id, OperationMode.Update);
                f.Show();
                this.Close();
            }
            else if (Mode =="deleteadvisor")
            {
                deleteAdvisor(sender, e);
            }
            else if(Mode =="updateproject")
            {
                Form f = new AddProject(id, OperationMode.Update);
                f.Show();
                this.Close();
            }
            else if(Mode=="deleteproject")
            {
                deleteproject(sender,e);
            }
            else if (Mode == "updateEvaluation")
            {
                Form f = new AddEvaluation(id, OperationMode.Update);
                f.Show();
                this.Close();
            }
            else if (Mode == "deleteEvaluation")
            {
                deleteEvaluation(sender, e);
            }
            



        }
        private void deleteStudent(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();


            SqlCommand checkStatusCmd = new SqlCommand("SELECT Status FROM GroupStudent WHERE StudentId = @StudentId", con);
            checkStatusCmd.Parameters.AddWithValue("@StudentId", int.Parse(Idtxt.Text));

            int currentStatus = (int)checkStatusCmd.ExecuteScalar();


            if (currentStatus == 3)
            {

                SqlCommand updateStatusCmd = new SqlCommand("UPDATE GroupStudent SET Status = 4 WHERE StudentId = @StudentId", con);
                updateStatusCmd.Parameters.AddWithValue("@StudentId", int.Parse(Idtxt.Text));
                int rowsAffected = updateStatusCmd.ExecuteNonQuery();

                if (rowsAffected > 0)
                {
                    MessageBox.Show("Successfully set status to Inactive in GroupStudent");
                }
                else
                {
                    MessageBox.Show("No rows were updated. Student may not exist in GroupStudent or already has Inactive status.");
                }
            }
            else
            {
                MessageBox.Show("Student already has Inactive status in GroupStudent. No further action needed.");
            }
            
        }
        private void deleteAdvisor(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand checkAdvisorCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Person WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkAdvisorCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));

            int AdvisorExists = (int)checkAdvisorCmd.ExecuteScalar();

            if (AdvisorExists == 1)
            {
                SqlCommand checkStatusCmd = new SqlCommand("SELECT FirstName FROM Person WHERE Id = @Id", con);
                checkStatusCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));

                object result=checkStatusCmd.ExecuteScalar();
                string name = $"@{result}";

                SqlCommand updateAdvisorCmd = new SqlCommand("UPDATE Person SET FirstName = @FirstName WHERE Id = @Id", con);
                updateAdvisorCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));
                updateAdvisorCmd.Parameters.AddWithValue("@FirstName",name);

                updateAdvisorCmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Deleted Advisor");
            }
            else
            {
                MessageBox.Show("Advisor with the provided Id does not exist");
            }

        }
        private void deleteproject(object sender, EventArgs e)
        { 
            var con = Configuration.getInstance().getConnection();
            SqlCommand checkProjectCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Project WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkProjectCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));

            int projectExists = (int)checkProjectCmd.ExecuteScalar();
            if (projectExists == 1)
            {
                SqlCommand updateStatusCmd = new SqlCommand("SELECT Title FROM Project WHERE Id = @Id", con);
                updateStatusCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));
                string title = (string)updateStatusCmd.ExecuteScalar();
                SqlCommand updateProjectCmd = new SqlCommand("UPDATE Project SET Title= @Title WHERE Id = @Id", con);
                updateProjectCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));
                updateProjectCmd.Parameters.AddWithValue("@Title", $"@{title}");

                   updateProjectCmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully set status to Inactive in Project");
            }
            else
            {
                MessageBox.Show("Project already has Inactive status in P. No further action needed.");
            }
        }
        private void deleteEvaluation(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand checkProjectCmd = new SqlCommand("IF EXISTS (SELECT 1 FROM Evaluation WHERE Id = @Id) SELECT 1 ELSE SELECT 0", con);
            checkProjectCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));
            int projectExists = (int)checkProjectCmd.ExecuteScalar();
            if (projectExists == 1)
            {
                SqlCommand updateStatusCmd = new SqlCommand("SELECT Name FROM Evaluation WHERE Id = @Id", con);
                updateStatusCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));
                string name = (string)updateStatusCmd.ExecuteScalar();
                SqlCommand updateProjectCmd = new SqlCommand("UPDATE Evaluation SET Name= @Name WHERE Id = @Id", con);
                updateProjectCmd.Parameters.AddWithValue("@Id", int.Parse(Idtxt.Text));
                updateProjectCmd.Parameters.AddWithValue("@Name", $"@{name}");

                 updateProjectCmd.ExecuteNonQuery();
                 MessageBox.Show("Successfully Deleted Evaluation"); 
            }
            else
            {
                MessageBox.Show("Project already has Inactive status in P. No further action needed.");
            }
        }

    }
}
